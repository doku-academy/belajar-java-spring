package com.azkafadhli.belajarjavaspring.unittests;

import com.azkafadhli.belajarjavaspring.dtos.CreateUserRequest;
import com.azkafadhli.belajarjavaspring.dtos.CreateUserResponse;
import com.azkafadhli.belajarjavaspring.entities.User;
import com.azkafadhli.belajarjavaspring.exceptions.DataAlreadyExistException;
import com.azkafadhli.belajarjavaspring.repositories.UserRepository;
import com.azkafadhli.belajarjavaspring.services.UserService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.openMocks;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    UserRepository userRepository;

    @Spy
    ModelMapper modelMapper = new ModelMapper();

    @Spy
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Spy
    @InjectMocks
    UserService userService = new UserService();

    @BeforeEach
    void setup() {
        openMocks(this);
    }

    // success
    @Test
    public void givenValidRequest_whenCreateNewUser_thenShouldBeCreated() {
        // arrange
        User newUser = new User(
                "azka@gmail.com",
                "azka",
                "ramadhan",
                "1234567890"
        );
        Mockito.when(userRepository.save(Mockito.any(User.class)))
                .thenReturn(newUser);

        CreateUserRequest newUserRequest = modelMapper.map(newUser, CreateUserRequest.class);

        // act
        CreateUserResponse newUserResponse = userService.createNewUser(newUserRequest);

        // assert
        assertThat(newUserResponse.getEmail()).isEqualTo(newUser.getEmail());
        assertThat(newUserResponse.getFirstName()).isEqualTo(newUser.getFirstName());
        assertThat(newUserResponse.getLastName()).isEqualTo(newUser.getLastName());
    }

    // failed
    @Test(expected = DataAlreadyExistException.class)
    public void givenValidRequestUserAlreadyExist_whenCreateUser_thenShouldThrowError() {
        // arrange
        User newUser = new User(
                "azka@gmail.com",
                "azka",
                "ramadhan",
                "1234567890"
        );
        Mockito.when(userRepository.findByEmail(Mockito.any()))
                .thenReturn(Optional.of(newUser));

        CreateUserRequest newUserRequest = modelMapper.map(newUser, CreateUserRequest.class);

        // act
        userService.createNewUser(newUserRequest);
    }

    @Test
    public void givenValidRequest_whenGetUsers_thenShouldReturnUsers() {
        int offset = 0, limit = 1;
        var paging = PageRequest.of(offset, limit);

        var userList = List.of(
                new User(
                        "azka@gmail.com",
                        "azka",
                        "ramadhan",
                        "1234567890"
                )
        );
        var userPage = new PageImpl<User>(userList, paging, 2);

        Mockito.when(
                userRepository.findAll(Mockito.any(Pageable.class))
        ).thenReturn(userPage);

        var pageUser = userService.getUsers(paging);

        assertThat(pageUser.getRight().getTotalItems()).isEqualTo(2);
        assertThat(pageUser.getRight().getTotalPages()).isEqualTo(2);
        assertThat(pageUser.getLeft().size()).isEqualTo(1);
    }
}
