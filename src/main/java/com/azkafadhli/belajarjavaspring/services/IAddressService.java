package com.azkafadhli.belajarjavaspring.services;

import com.azkafadhli.belajarjavaspring.dtos.CreateAddressRequest;
import com.azkafadhli.belajarjavaspring.entities.Address;

public interface IAddressService {
    Address createNewAddress(Long userId, CreateAddressRequest req);
}
