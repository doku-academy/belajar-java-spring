package com.azkafadhli.belajarjavaspring.services;

import com.azkafadhli.belajarjavaspring.dtos.CreateUserRequest;
import com.azkafadhli.belajarjavaspring.dtos.CreateUserResponse;
import com.azkafadhli.belajarjavaspring.dtos.Pagination;
import com.azkafadhli.belajarjavaspring.dtos.UserDetailsResponse;
import com.azkafadhli.belajarjavaspring.entities.User;
import com.azkafadhli.belajarjavaspring.exceptions.DataAlreadyExistException;
import com.azkafadhli.belajarjavaspring.exceptions.DataNotExistException;
import com.azkafadhli.belajarjavaspring.repositories.UserRepository;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    private User convertToEntity(CreateUserRequest req) {
        return modelMapper.map(req, User.class);
    }

    private CreateUserResponse convertToDto(User user) {
        return modelMapper.map(user, CreateUserResponse.class);
    }

    @Transactional
    public CreateUserResponse createNewUser(CreateUserRequest req) {

        User user = convertToEntity(req);

        Optional<User> userOptional = userRepository.findByEmail(req.getEmail());
        if (userOptional.isPresent()) {
            throw new DataAlreadyExistException("Data with Key (email)=("+req.getEmail()+") already exists.");
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User createdUser = userRepository.save(user);
        return convertToDto(createdUser);
    }


    @Cacheable(value = "users")
    public Pair<List<CreateUserResponse>, Pagination> getUsers(Pageable paging) {
        Page<User> pageUser = userRepository.findAll(paging);

        List<CreateUserResponse> usersDto = pageUser
                .getContent()
                .stream()
                .map(user -> convertToDto(user))
                .collect(Collectors.toList());

        Pagination pagination = new Pagination(
                pageUser.getTotalPages(),
                pageUser.getTotalElements(),
                pageUser.getNumber() + 1 // offset is starting from 0, so add to get current page
        );
        Pair res = new ImmutablePair(usersDto, pagination);
        return res;
    }

    public UserDetailsResponse getUserDetails(Long userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new DataNotExistException("Data with Key (id)="+userId+"not exists.");
        }

        User user = userOptional.get();
        return modelMapper.map(user, UserDetailsResponse.class);
    }
}
