package com.azkafadhli.belajarjavaspring.services;

import com.azkafadhli.belajarjavaspring.dtos.CreateUserRequest;
import com.azkafadhli.belajarjavaspring.dtos.CreateUserResponse;
import com.azkafadhli.belajarjavaspring.dtos.Pagination;
import com.azkafadhli.belajarjavaspring.dtos.UserDetailsResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface IUserService {
    CreateUserResponse createNewUser(CreateUserRequest req);
    Pair<List<CreateUserResponse>, Pagination> getUsers(Pageable paging);
    UserDetailsResponse getUserDetails(Long userId);
}
