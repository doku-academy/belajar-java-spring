package com.azkafadhli.belajarjavaspring.services;

import com.azkafadhli.belajarjavaspring.dtos.CreateRoleRequest;
import com.azkafadhli.belajarjavaspring.dtos.RoleResponse;
import com.azkafadhli.belajarjavaspring.entities.Role;

import java.util.List;

public interface IRoleService {
    List<RoleResponse> getRoles();
    Role createNewRole(CreateRoleRequest role);
}
