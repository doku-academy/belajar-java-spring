package com.azkafadhli.belajarjavaspring.services;

import com.azkafadhli.belajarjavaspring.dtos.CreateRoleRequest;
import com.azkafadhli.belajarjavaspring.dtos.RoleResponse;
import com.azkafadhli.belajarjavaspring.entities.Role;
import com.azkafadhli.belajarjavaspring.repositories.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService implements IRoleService{
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ModelMapper modelMapper;

    public List<RoleResponse> getRoles() {
        var roles = roleRepository.findAll();
        var rolesDTO = roles
                .stream()
                .map(role -> modelMapper.map(role, RoleResponse.class))
                .collect(Collectors.toList());

        return rolesDTO;
    }

    public Role createNewRole(CreateRoleRequest req) {
        Role role = modelMapper.map(req, Role.class);
        Role createdRole = roleRepository.save(role);
        return createdRole;
    }
}
