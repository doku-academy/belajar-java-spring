package com.azkafadhli.belajarjavaspring.services;

import com.azkafadhli.belajarjavaspring.dtos.CreateAddressRequest;
import com.azkafadhli.belajarjavaspring.entities.Address;
import com.azkafadhli.belajarjavaspring.entities.User;
import com.azkafadhli.belajarjavaspring.repositories.AddressRepository;
import com.azkafadhli.belajarjavaspring.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService implements IAddressService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressRepository addressRepository;

    public Address createNewAddress(Long userId, CreateAddressRequest req) {
        Optional<User> userOptional = userRepository.findById(userId);
        User user = userOptional.get();
        Address address = new Address(req.getAddress(), user);
        Address createdAddress = addressRepository.save(address);
        return createdAddress;
    }
}
