package com.azkafadhli.belajarjavaspring.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
@Slf4j
public class TokenService {
    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;
    @Autowired
    JwtEncoder encoder;

    public String generateToken(Authentication authentication) {
        Instant now = Instant.now(); // timestamp now

        JwtClaimsSet claims = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(Instant.now())
                .expiresAt(now.plus(this.jwtExpirationInMs, ChronoUnit.MILLIS))
                .subject(authentication.getName())
                .build();

        var token = this.encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();

        return token;
    }
}
