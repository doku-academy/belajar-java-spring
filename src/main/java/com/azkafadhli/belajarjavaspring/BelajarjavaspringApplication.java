package com.azkafadhli.belajarjavaspring;

//import com.azkafadhli.belajarjavaspring.entities.User;
//import com.azkafadhli.belajarjavaspring.repositories.UserRepository;
//import org.springframework.boot.CommandLineRunner;
import com.azkafadhli.belajarjavaspring.securities.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class BelajarjavaspringApplication {

	public static void main(String[] args) {

		SpringApplication.run(BelajarjavaspringApplication.class, args);

	}

//	@Bean
//	public CommandLineRunner run(UserRepository r) {
//		return (args -> {insertUserDummyData(r);});
//	}

//	private static void insertUserDummyData(UserRepository r) {
//		r.save(new User("azka@gmail.com", "azka", "ramadhan", "abc"));
//	}
}
