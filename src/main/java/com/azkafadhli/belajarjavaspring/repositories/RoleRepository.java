package com.azkafadhli.belajarjavaspring.repositories;

import com.azkafadhli.belajarjavaspring.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role, Long> {
}
