package com.azkafadhli.belajarjavaspring.repositories;

import com.azkafadhli.belajarjavaspring.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {}
