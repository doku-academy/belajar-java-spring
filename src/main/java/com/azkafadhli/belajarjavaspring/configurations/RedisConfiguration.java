package com.azkafadhli.belajarjavaspring.configurations;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class RedisConfiguration {
//    @Bean
//    public LettuceConnectionFactory lettuceConnectionFactory() {
//        var redisConf = new RedisStandaloneConfiguration();
//        redisConf.setDatabase(0);

//        if you need to configure the connection details,
//        you can modify the default RedisStandaloneConfiguration
//        redisStandaloneConfiguration.setHostName("localhost");
//        redisStandaloneConfiguration.setPort(6379);
//        redisStandaloneConfiguration.setUsername(null);
//        redisStandaloneConfiguration.setPassword(RedisPassword.none());

//        return new LettuceConnectionFactory(redisConf);
//    }

//    @Bean
//    public RedisTemplate<String, Object> redisTemplate() {
//        RedisTemplate<String, Object> template = new RedisTemplate<>();
//        template.setConnectionFactory(lettuceConnectionFactory());
//        return template;
//    }
}
