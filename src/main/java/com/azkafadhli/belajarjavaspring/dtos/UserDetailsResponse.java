package com.azkafadhli.belajarjavaspring.dtos;

import com.azkafadhli.belajarjavaspring.entities.Address;
import com.azkafadhli.belajarjavaspring.entities.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class UserDetailsResponse {
    @JsonProperty("user_id")
    @ApiModelProperty(notes="The database generated user id")
    private Long id;

    @JsonProperty("first_name")
    @ApiModelProperty(notes="User's first name", required = true)
    private String firstName;

    @JsonProperty("last_name")
    @ApiModelProperty(notes="User's last name", required = true)
    private String lastName;

    @ApiModelProperty(notes="User's email", required = true)
    private String email;

    @JsonIgnore
    private String password;

    private Set<Role> roles;

    private List<Address> addresses;
}
