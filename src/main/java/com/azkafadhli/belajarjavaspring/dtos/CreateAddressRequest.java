package com.azkafadhli.belajarjavaspring.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateAddressRequest implements Serializable {
    private String address;
}
