package com.azkafadhli.belajarjavaspring.dtos;

import org.springframework.http.HttpStatus;

public class SuccessResponse <T> extends BaseResponse{
    public SuccessResponse(T data) {
        super(HttpStatus.OK.value(), "success", data);
    }
    public SuccessResponse(T data, Pagination pagination) {
        super(HttpStatus.OK.value(), "success", data, pagination);
    }
}
