package com.azkafadhli.belajarjavaspring.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Pagination implements Serializable {
    @JsonProperty("total_pages")
    @ApiModelProperty(example = "1")
    private int totalPages;

    @JsonProperty("total_items")
    @ApiModelProperty(example = "1")
    private Long totalItems;

    @JsonProperty("current_page")
    @ApiModelProperty(example = "1")
    private int currentPage;

    public Pagination(int totalPages, Long totalItems, int currentPage) {
        this.totalPages = totalPages;
        this.totalItems = totalItems;
        this.currentPage = currentPage;
    }
}
