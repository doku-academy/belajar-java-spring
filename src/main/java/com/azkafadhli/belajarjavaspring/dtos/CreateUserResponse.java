package com.azkafadhli.belajarjavaspring.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateUserResponse implements Serializable {
    @JsonProperty("user_id")
    @ApiModelProperty(notes="The database generated user id", example = "1")
    private Long id;

    @JsonProperty("first_name")
    @ApiModelProperty(notes="User's first name", example = "John")
    private String firstName;

    @JsonProperty("last_name")
    @ApiModelProperty(notes="User's last name", example = "Doe")
    private String lastName;

    @ApiModelProperty(notes="User's email", example = "johndoe@mail.com")
    private String email;
}
