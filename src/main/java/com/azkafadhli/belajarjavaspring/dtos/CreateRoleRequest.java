package com.azkafadhli.belajarjavaspring.dtos;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateRoleRequest implements Serializable {
    private String role;
}
