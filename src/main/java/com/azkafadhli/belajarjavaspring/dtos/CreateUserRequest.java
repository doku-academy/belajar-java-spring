package com.azkafadhli.belajarjavaspring.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateUserRequest implements Serializable {
    @JsonProperty(required = true)
    private String email;

    @JsonProperty(value = "first_name", required = true)
    private String firstName;

    @JsonProperty(value = "last_name", required = true)
    private String lastName;

    @JsonProperty(required = true)
    private String username;

    private Boolean enabled = false;

    @JsonProperty(required = true)
    private String password;
}
