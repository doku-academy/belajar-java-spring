package com.azkafadhli.belajarjavaspring.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.BAD_REQUEST, reason="Data not found")
public class DataNotExistException extends RuntimeException {
    public DataNotExistException() {
        super();
    }

    public DataNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataNotExistException(String message) {
        super(message);
    }

    public DataNotExistException(Throwable cause) {
        super(cause);
    }
}
