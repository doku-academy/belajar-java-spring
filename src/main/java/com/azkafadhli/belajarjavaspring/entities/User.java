package com.azkafadhli.belajarjavaspring.entities;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity // indicating JPA's entity
@Table(name = "users")
@Getter
@Setter
public class User {
    // JPA recognize this property as object's ID
    @Id
    // ID will be generated automatically
    // IDENTITY means that ID generation will be maintained by database, SEQUENCE by application
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // email & name will be mapped to column with same name
    // if no name set in annotation @Column
    @Column(unique = true, nullable = false)
    private String email;

    private String firstName;

    private String lastName;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(columnDefinition = "boolean default false")
    private Boolean enabled;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_roles",
            joinColumns = { @JoinColumn(name="user_id") },
            inverseJoinColumns = { @JoinColumn(name="role_id") }
    )
    private Set<Role> roles;

    @OneToMany(mappedBy = "user")
    private List<Address> addresses;

    // default constructor for JPA
    protected User() {}

    // constructor use to create instances of User to be saved to database
    public User(String email, String firstName, String lastName, String password) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format(
                "User{id=%s, email=%s, firstName=%s, lastName=%s}",
                id, email, firstName, lastName
        );
    }
}




