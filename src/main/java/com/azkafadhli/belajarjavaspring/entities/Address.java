package com.azkafadhli.belajarjavaspring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "addresses")
@Getter
@Setter
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TEXT")
    private String address;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private User user;

    protected Address() {}

    public Address(String address, User user) {
        this.address = address;
        this.user = user;
    }

    @Override
    public String toString() {
        return String.format("Address{id=%s, address=%s, user=%s}", id, address, user);
    }
}
