package com.azkafadhli.belajarjavaspring.controllers;

import com.azkafadhli.belajarjavaspring.dtos.BaseResponse;
import com.azkafadhli.belajarjavaspring.dtos.CreateRoleRequest;
import com.azkafadhli.belajarjavaspring.dtos.SuccessResponse;
import com.azkafadhli.belajarjavaspring.entities.Role;
import com.azkafadhli.belajarjavaspring.services.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/v1")
public class RoleController {
    @Autowired
    IRoleService roleService;

    @GetMapping("/roles")
    public ResponseEntity<BaseResponse> getRoles() {
        SuccessResponse res = new SuccessResponse(
                roleService.getRoles()
        );
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping("/roles")
    public ResponseEntity<BaseResponse> createRoles(
            @RequestBody CreateRoleRequest req
            ) {
        Role res = roleService.createNewRole(req);
        return new ResponseEntity<>(new SuccessResponse(res), HttpStatus.CREATED);
    }
}
