package com.azkafadhli.belajarjavaspring.controllers;

import com.azkafadhli.belajarjavaspring.dtos.*;
import com.azkafadhli.belajarjavaspring.services.IUserService;
import com.azkafadhli.belajarjavaspring.services.TokenService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.AbstractOAuth2TokenAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class UserController {

    @Autowired
    IUserService userService;

    @Autowired
    TokenService tokenService;

    @ApiOperation(value="v1 create new user", produces = "application/json", notes = "Email and username must unique")
    @ApiResponses({
            @ApiResponse(
                    code=201,
                    message="Successfully create new user",
                    reference = "#/definitions/SuccessResponse«CreateUserResponse»"
            )
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/auth/register")
    public ResponseEntity<SuccessResponse<CreateUserResponse>> createNewUser(@RequestBody CreateUserRequest req) {
        CreateUserResponse userRes = userService.createNewUser(req);
        var res = new ResponseEntity<SuccessResponse<CreateUserResponse>>(
                new SuccessResponse(userRes), HttpStatus.CREATED);

        return res;
    }

    @ApiOperation(value="Get all users", produces = "application/json")
    @GetMapping("/v1/users")
    public ResponseEntity<BaseResponse> getUsers(
        @RequestParam(name="ids", required = false) List<Long> ids,
        @RequestParam(name="first_name", required = false) String firstName,
        @RequestParam(name="last_name", required = false) String lastName,
        @RequestParam(name="page", defaultValue = "1") int page,
        @RequestParam(name="limit", defaultValue = "10") int limit,
        AbstractOAuth2TokenAuthenticationToken authentication
    ) {
        int offset = page - 1;
        Pageable paging = PageRequest.of(offset, limit);

        // TODO: filtering by ids, first name, last name


        var name = authentication.getName();
        var credentials = authentication.getCredentials();
        var authorities = authentication.getAuthorities();
        var details = authentication.getDetails();
        var principal = authentication.getPrincipal();
        var tokenAttr = authentication.getTokenAttributes();
        log.info(String.format(
                "name: %s, credentials: %s, authorities: %s, details: %s",
                name, credentials, authorities, credentials, details
        ));
        log.info(String.format("Token attribute; %s", tokenAttr));


        Pair<List<CreateUserResponse>,Pagination> usersAndPagination = userService.getUsers(paging);
        SuccessResponse res = new SuccessResponse(usersAndPagination.getLeft(), usersAndPagination.getRight());

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @ApiOperation(value="Get user details", produces = "application/json")
    @GetMapping("/v1/users/{userId}")
    public ResponseEntity<BaseResponse> getUserAddresses(
            @PathVariable("userId") Long userId
    ) {
        UserDetailsResponse user = userService.getUserDetails(userId);
        return new ResponseEntity<>(new SuccessResponse(user), HttpStatus.OK);
    }
}
