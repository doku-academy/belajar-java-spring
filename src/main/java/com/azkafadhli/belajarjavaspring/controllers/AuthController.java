package com.azkafadhli.belajarjavaspring.controllers;

import com.azkafadhli.belajarjavaspring.dtos.JwtAuthenticationResponse;
import com.azkafadhli.belajarjavaspring.dtos.LoginRequest;
import com.azkafadhli.belajarjavaspring.services.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AuthController {

    @Autowired
    TokenService tokenService;

    @Autowired
    AuthenticationManager authenticationManager;

    @PostMapping("/auth/token")
    public ResponseEntity<?> token(
            @RequestBody LoginRequest req
    ) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        req.getUsername(),
                        req.getPassword()
                )
        );

        log.info(authentication.toString());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenService.generateToken(authentication);

//        log.trace("a trace message");
//        log.debug("a debug message");
//        log.info("an info message");
//        log.warn("a warn message");
//        log.error("an error message");

        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

}
