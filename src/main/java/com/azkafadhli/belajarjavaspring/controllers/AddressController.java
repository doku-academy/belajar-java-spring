package com.azkafadhli.belajarjavaspring.controllers;

import com.azkafadhli.belajarjavaspring.dtos.BaseResponse;
import com.azkafadhli.belajarjavaspring.dtos.CreateAddressRequest;
import com.azkafadhli.belajarjavaspring.dtos.SuccessResponse;
import com.azkafadhli.belajarjavaspring.entities.Address;
import com.azkafadhli.belajarjavaspring.services.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AddressController {
    @Autowired
    IAddressService addressService;

    @PostMapping(value = "/v1/users/{userId}/addresses", produces = "application/json")
    public ResponseEntity<BaseResponse> createNewAddress(
            @PathVariable("userId") Long userId,
            @RequestBody CreateAddressRequest req
        ) {
        Address res = addressService.createNewAddress(userId, req);
        return new ResponseEntity<>(new SuccessResponse(res), HttpStatus.CREATED);
    }
}
